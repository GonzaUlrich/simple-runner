﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {

	[SerializeField]
	private Transform m_RaceCenter;
	[SerializeField]
	private float m_SecondsByLap;
	[SerializeField]
	private float m_CurrentSpeed = 1f;
	[SerializeField]
	private float m_SideSpeed = 20f;
	[SerializeField]
	private float m_MaxDistance = 2f;
	[SerializeField]
	private float m_MinDistance = 1f;
	private int m_CurrentLap = 0;
	private bool m_Running = true;
	private int m_TotalLaps = 3;
	
	private void Update () {
		if (!m_Running) {
			return;
		}
		float angles = 360f / m_SecondsByLap;
		transform.RotateAround (m_RaceCenter.position, Vector3.up, angles * Time.deltaTime * m_CurrentSpeed);
		float inputHorizontal = Input.GetAxis ("Horizontal");
		float xtranslation = inputHorizontal * Time.deltaTime * m_SideSpeed;
		float distance = Vector3.Distance (m_RaceCenter.position, transform.position);
		if (inputHorizontal > 0f && distance > m_MinDistance || inputHorizontal < 0f && distance < m_MaxDistance) {
			transform.Translate (new Vector3 (xtranslation, 0f, 0f));
		}
	}

	private void OnTriggerEnter (Collider collider){
		if (collider.gameObject.tag == "CheckPoint") {
			m_CurrentLap++;
			if (m_CurrentLap >= m_TotalLaps) {
				EndRace ();
			}
		}
	}

	private void EndRace(){
		m_Running = false;
	}
}